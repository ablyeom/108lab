var form = $("#wizard");

function showProgress() {
    $("#progress-view").fadeIn();
}

function hideProgress() {
    setTimeout(function() {
        $("#progress-view").fadeOut();
    }, 500);
}

var calc = {
    total: 0,
    spicySum: 0,
    type: 0,
    spices: [],
    push: function (title, value) {
        if (value === 0 || title === '') {
            return;
        }

        var newValue = this.type === 0 ? value / 100 * this.spicySum : value;
        var rate = this.type === 0 ? value / 100 : value / this.spicySum;

        this.spices.push(new Spicy(title, newValue, rate));
    },
};

function Allergen(key, rate) {
    this.key = key;
    this.rate = rate;
}

function Spicy(key, value, rate) {
    this.key = key;
    this.value = value;
    this.rate = rate;
    this.allergens = {};

    for (var i = 0; i < 25; i++) {
        var key = String.fromCharCode(97 + i);
        this.allergens[key] = new Allergen(key, 0);
    }
}

// $(document).ready(function () {

    function getParameterByName(name, url) {
        if (!url) url = window.location.href;
        name = name.replace(/[\[\]]/g, '\\$&');
        var regex = new RegExp('[?&]' + name + '(=([^&#]*)|&|#|$)'),
            results = regex.exec(url);
        if (!results) return null;
        if (!results[2]) return '';
        return decodeURIComponent(results[2].replace(/\+/g, ' '));
    }

    $('input[type=number]').on('focus', function (e) {
        $(this).on('wheel.disableScroll', function (e) {
            e.preventDefault()
        })
    }).on('blur', function (e) {
        $(this).off('wheel.disableScroll')
    });

    function showResult() {

        for (var index = 0; index < calc.spices.length; index++) {
            for (var i = 0; i < 25; i++) {
                var key = String.fromCharCode(97 + i);
                var value = $("input[name='" + key + "[" + index + "]']").val() * 1;
                var allergen = calc.spices[index].allergens[key];
                allergen.rate = value * 0.01;
                calc.spices[index].allergens[key] = allergen;
            }
        }

        var result = {};

        for (var index = 0; index < calc.spices.length; index++) {
            var spice = calc.spices[index];
            for (var key in spice.allergens) {

                var origin = result[key];
                if (origin === undefined) {
                    origin = {
                        value: 0
                    }
                }

                origin.value = origin.value + (spice.rate * spice.allergens[key].rate * calc.spicySum / calc.total);

                result[key] = origin;
            }
        }

        for (var jIndex in result) {
            var rate = result[jIndex];
            var node = $("#table-result input[name='" + jIndex + "']").val(rate.value * 100);

            if (jIndex === 'b') {
                if (rate.value > 0.01) {
                    node.closest("tr").find("td:last-child").html("배합금지")
                }
            }
            else if (rate.value > 0.0001) {
                if (jIndex === 'v') {
                    node.closest("tr").find("td:last-child").html('배합금지')
                } else {
                    node.closest("tr").find("td:last-child").html('표시')
                }
            }
        }
    }

    function hideColumn(c) {
        for (var i = 0; i < 25; i++) {
            $("input[name='" + String.fromCharCode(97 + i) + "[" + c + "]']").attr('disabled', true).parent().hide();
        }
    }

    // var form = $("#wizard");

    form.steps({
        headerTag: "h3",
        bodyTag: "section",
        transitionEffect: 3,
        saveState: true,
        autoFocus: true,
        startIndex: getParameterByName("step", window.location) * 1,
        labels: {
            previous: '<img src="./img/previous.png" style="width: 40px; height: 40px;" alt="Previous">',
            next: '<img src="./img/next.png" style="width: 40px; height: 40px;" alt="Next">',
            cancel: '취소',
            current: '',
            pagination: '',
            loading: '로딩 중...',
            finish: "저장"
        },
        onInit: function(event, currentIndex) {
            hideProgress();
        },
        onStepChanging: function (event, currentIndex, newIndex) {

            showProgress();

            if (newIndex > 0) {
                $("#startover").show();
            }
            else {
                $("#startover").hide();
            }

            if (currentIndex > newIndex) return true;

            if (newIndex === 1) {
                calc.total = $("input[name='total']").val() * 1;
                calc.spicySum = $("input[name='spicyWeight']").val() * 1;

                if (calc.total === 0) {
                    alert("비누의 양을 입력해주세요.");
                    $("input[name='total']").focus();
                    hideProgress();
                    return false;
                }

                if (calc.spicySum === 0) {
                    alert("향료의 양을 입력해주세요.");
                    $("input[name='spicyWeight']").focus();
                    hideProgress();
                    return false;
                }

                $("input[name='type']").on('change', function () {
                    form.steps('next');
                });

                return true;
            }

            if (newIndex === 2) {
                calc.type = $("input[name='type']:checked").val() * 1;
                if (calc.type === 0) {
                    $("#spices .input-group-append .input-group-text").html('%');
                } else {
                    $("#spices .input-group-append .input-group-text").html('g');
                }
                return true;
            }

            if (newIndex === 3) {
                calc.spices = [];

                var t1 = $("input[name='spicy[0]']").val();
                var r1 = $("input[name='spicyRate[0]']").val() * 1;
                calc.push(t1, r1);

                var t2 = $("input[name='spicy[1]']").val();
                var r2 = $("input[name='spicyRate[1]']").val() * 1;
                calc.push(t2, r2);

                var t3 = $("input[name='spicy[2]']").val();
                var r3 = $("input[name='spicyRate[2]']").val() * 1;
                calc.push(t3, r3);

                var t4 = $("input[name='spicy[3]']").val();
                var r4 = $("input[name='spicyRate[3]']").val() * 1;
                calc.push(t4, r4);

                var t5 = $("input[name='spicy[4]']").val();
                var r5 = $("input[name='spicyRate[4]']").val() * 1;
                calc.push(t5, r5);

                var t6 = $("input[name='spicy[5]']").val();
                var r6 = $("input[name='spicyRate[5]']").val() * 1;
                calc.push(t6, r6);

                if (calc.spices.length === 0) {
                    alert('최소 1가지 이상의 향을 입력하셔야 됩니다.');
                    return false;
                }

                var sum = 0;

                for (var index in calc.spices) {
                    sum += calc.spices[index].value;
                }

                if (sum !== calc.spicySum) {
                    if (calc.type === 0) {
                        alert('배합의 합이 100%를 초과하거나 미만일 수 없습니다.');
                    } else {
                        alert('입력하신 향의 배합 합은 ' + sum + '입니다. 합이 ' + calc.spicySum + '를 초과하거나 미만일 수 없습니다.');
                    }
                    hideProgress();
                    return false;
                }

                if (calc.spices.length > 3 && calc.spices.length < 6) {
                    console.info('hide column 6');
                    hideColumn(5);
                    $("#table-s2 thead th.spicy-column:nth-child(4)").hide();

                    if (calc.spices.length === 4) {
                        console.info('hide column 6');
                        hideColumn(4);
                        $("#table-s2 thead th.spicy-column:nth-child(3)").hide();
                    }
                }
                else if (calc.spices.length < 3) {
                    console.info('hide column 2');
                    hideColumn(2);

                    $("#table-s1 thead th.spicy-column:nth-child(4)").hide();

                    if (calc.spices.length === 1) {
                        console.info('hide column 1');
                        $("#table-s1 thead th.spicy-column:nth-child(3)").hide();
                        hideColumn(1);
                    }
                }
                return true;
            }

            if (newIndex === 4) {
                if (calc.spices.length <= 3) {
                    showResult();
                }

                return true;
            }

            showResult();

            return (currentIndex < newIndex && (form.find(".body:eq(" + newIndex + ") label.error").remove(), form.find(".body:eq(" + newIndex + ") .error").removeClass("error")), form.validate().settings.ignore = ":disabled,:hidden", form.valid())
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            hideProgress();
            if (currentIndex === 4 && calc.spices.length <= 3) {
                if (priorIndex === 5) {
                    form.steps('previous');
                    return;
                }
                form.steps("next");
            }
        },
        onFinishing: function (event, currentIndex) {
            return form.validate().settings.ignore = ":disabled", form.valid()
        },
        onFinished: function (event, currentIndex) {
        }
    });

    form.validate({
        ignore: "input[type=hidden]",
        errorClass: "text-danger col-12 px-0",
        successClass: "text-success",
        highlight: function (element, errorClass) {
            $(element).removeClass(errorClass)
        },
        unhighlight: function (element, errorClass) {
            $(element).removeClass(errorClass)
        },
        errorPlacement: function (error, element) {
            error.insertAfter(element.parent())
        },
        invalidHandler: function (event, validator) {
            // 'this' refers to the form
            var errors = validator.numberOfInvalids();
            if (errors) {
                var message = errors === 1 ? 'You missed 1 field. It has been highlighted' : 'You missed ' + errors + ' fields. They have been highlighted';
                console.debug(validator);
            }
        }
    });

    $.extend($.validator.messages, {
        required: "필수 항목입니다.",
        remote: "항목을 수정하세요.",
        email: "유효하지 않은 E-Mail주소입니다.",
        url: "유효하지 않은 URL입니다.",
        date: "올바른 날짜를 입력하세요.",
        dateISO: "올바른 날짜(ISO)를 입력하세요.",
        number: "유효한 숫자가 아닙니다.",
        digits: "숫자만 입력 가능합니다.",
        creditcard: "신용카드 번호가 바르지 않습니다.",
        equalTo: "같은 값을 다시 입력하세요.",
        extension: "올바른 확장자가 아닙니다.",
        maxlength: $.validator.format("{0}자를 넘을 수 없습니다. "),
        minlength: $.validator.format("{0}자 이상 입력하세요."),
        rangelength: $.validator.format("문자 길이가 {0} 에서 {1} 사이의 값을 입력하세요."),
        range: $.validator.format("{0} 에서 {1} 사이의 값을 입력하세요."),
        max: $.validator.format("{0} 이하의 값을 입력하세요."),
        min: $.validator.format("{0} 이상의 값을 입력하세요.")
    });
// })